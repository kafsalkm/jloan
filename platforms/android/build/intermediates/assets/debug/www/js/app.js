// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter.controllers', []);

angular.module('app', ['ionic', 'starter.controllers','starter.services'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
	controller: 'loginCtrl'
  })

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.loans', {
      url: '/loans',
      views: {
        'menuContent': {
          templateUrl: 'templates/loans.html',
          controller: 'loanCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/loans/:title',
    views: {
      'menuContent': {
        templateUrl: 'templates/loanDetails.html',
        controller: 'loan'
      }
    }
  })
  .state('app.myDetails', {
    url: '/myDetails',
    views: {
      'menuContent': {
        templateUrl: 'templates/myDetails.html',
        controller: 'myDetails'
      }
    }
  }).state('app.loanReport', {
    url: '/loanReport',
    views: {
      'menuContent': {
        templateUrl: 'templates/loanReport.html',
        controller: 'loanReport'
      }
    }
  })
  .state('app.uploadDocs', {
    url: '/uploadDocuments',
    views: {
      'menuContent': {
        templateUrl: 'templates/uploadDocs.html',
        controller: 'uploadDocs'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
