app.controller('AppCtrl', function($scope, $state, $ionicModal, $timeout, $window, $ionicHistory, $ionicLoading) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  //
  $scope.username = $window.localStorage['userName'];
  $scope.lastname = $window.localStorage['lastName'];	

	$scope.logout = function logout() {
		$scope.showLoading();
		$window.localStorage.clear();
		$ionicHistory.clearCache();
    	$ionicHistory.clearHistory();
    	$scope = $scope.$new(true);
    	delete $scope;
		$state.go('login');
		location.reload();
		$scope.hideLoading();	
	}

	$scope.myDetail = function myDetail() {

		$state.go('app.myDetails');
	}
	
	$scope.showLoading = function() {
		$ionicLoading.show({
			template: '<ion-spinner icon="android" class="spinner"></ion-spinner>  <span>Loging Out...</span>'
		});
	};
	
	$scope.hideLoading = function(){
		$ionicLoading.hide();
	};
});





