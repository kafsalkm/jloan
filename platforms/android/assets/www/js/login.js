
app.controller('loginCtrl', function($scope, $state, $ionicPopup, $window, loanService, $ionicLoading) {

	$scope.doLogin = function (loginData){
	$scope.showLoading();
	if(!loginData){
		var alertPopup = $ionicPopup.alert({
			title: 'Wrong Credentials',
			template: 'Please enter Username and Password!'
   });
	}
	else{
		$scope.isValid = loanService.login(loginData);

		if($scope.isValid == true){

			$state.go('app.loans');
		}

		else{
			$scope.showAlert();
		}
	}
	$scope.hideLoading();
	};
	// An alert dialog
 $scope.showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Wrong Credentials',
     template: 'The username and passowrd you entered don\'t match!'
   });
   };

   $scope.allImages = [{
   		'src' : 'img/loan1.jpg'
   	}, {
   		'src' : 'img/loan2.jpg'
   	}, {
   		'src' : 'img/loan3.jpg'
	},{
   		'src' : 'img/loan4.jpg'
	}];
	
	$scope.showLoading = function() {
		$ionicLoading.show({
			template: '<ion-spinner icon="android" class="spinner"></ion-spinner>  <span>Loading...</span>'
		});
	};
	
	$scope.hideLoading = function(){
		$ionicLoading.hide();
	};
});
