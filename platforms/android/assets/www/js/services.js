
angular.module('starter.services', [])
.service('loanService', function($http, $window) {
  // Might use a resource here that returns a JSON array

  var getUIFactory = {};

// 1.1 get list of loans
	var _getmyLoans = function () {

		return loans;
    };

//1.0 user login validation
    var _validateuser = function (userDet) {

		var isValid = 0;
		for(user in userslist){

			if(userslist[user].username == userDet.username && userslist[user].password == userDet.password){

					$window.localStorage['userId'] = userslist[user].userid;
					$window.localStorage['userName'] = userslist[user].username;
					$window.localStorage['lastName'] = userslist[user].lastname;
					isValid++;
			}
			else{
				continue;
			}
		}
		if(isValid > 0)
			return true;
		else
			return false;

    };

	getUIFactory.getmyLoans = _getmyLoans;
	getUIFactory.login = _validateuser;

    return getUIFactory;
});
