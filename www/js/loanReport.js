app.controller('loanReport', ['$scope', '$state', '$window', function($scope, $state, $window) {

  	$scope.userId = $window.localStorage['userId'];
	$scope.btn = true;
	$scope.form = true;
	$scope.loanReport = {
		"loanPurpose" : $scope.loanPurpose,
		"firstBuyer" : $scope.firstBuyer,
		"propertyType" : $scope.propertyType,
		"loanNeedIn" : $scope.loanNeedIn,
		"price" : $scope.price,
		"location" : $scope.location,
		"funds" : $scope.funds,
		"incomeType" : $scope.incomeType,
		"grossIncome" : $scope.grossIncome,
		"otherIncomes" : $scope.otherIncomes,
		"repayments" : $scope.repayments,
		"creditHistory" : $scope.creditHistory,
		"employmentType" : $scope.employmentType,
		"firstname" : $scope.fname,
		"lastname" : $scope.lname,
		"postcode" : $scope.postcode,
		"mobileno" : $scope.mobileno,
		"emailid" : $scope.emailid,
		"contactPreference" : $scope.contactPreference
	};
	
	//get selected options for loan report
	 $(document).ready(function(){
        $("div").click(function(){
		if (this.id != "") {
			var divid = this.id;
			var value = this.innerText;
			value = value.trim();
			var model = this.attributes.name.value;
			if(model == "loanPurpose"){
				$('#111').removeClass('app_button_red');
				$('#112').removeClass('app_button_red');
				$scope.loanReport.loanPurpose = value;
			}
			if(model == "firstBuyer"){
				$scope.loanReport.firstBuyer = value;
				$('#221').removeClass('app_button_red');
				$('#222').removeClass('app_button_red');
			}
			if(model == "propertyType"){
				$scope.loanReport.propertyType = value;
				$('#331').removeClass('app_button_red');
				$('#332').removeClass('app_button_red');
			}
			if(model == "loanNeedIn"){
				$scope.loanReport.loanNeedIn = value;
				for(var i=441; i<445; i++){
					$('#'+i).removeClass('app_button_red');
				}
			}
			if(model == "incomeType"){
				$scope.loanReport.incomeType = value;
				$('#661').removeClass('app_button_red');
				$('#662').removeClass('app_button_red');
			}
			if(model == "creditHistory"){
				$scope.loanReport.creditHistory =value;
				$('#881').removeClass('app_button_red');
				$('#882').removeClass('app_button_red');
			}
			if(model == "employmentType"){
				$scope.loanReport.employmentType = value;
				for(var i=991; i<997; i++){
					$('#'+i).removeClass('app_button_red');
				}
			}
			$scope.next();
		}
        $('#'+divid).addClass('app_button_red');
		
		});

    });
  	var isValid = 0;
	var i =0;
		
	$scope.divs = [1,2,3,4,5,6,7,8,9,10,11];
	
		$scope.next = function() {
			var current = $scope.divs[i];
			i = i + 1;
			var next = $scope.divs[i];
			
			if (i == 1){
				$('#'+current).addClass('hide');	
				$('#'+next).removeClass('hide'); 
				$('#previuos').removeClass('hide');
			}
			else if(i == 10){
				$('#'+current).addClass('hide');	
				$('#'+next).removeClass('hide'); 
				$('#next').addClass('hide'); 
			}
			else{
				$('#'+current).addClass('hide');	
				$('#'+next).removeClass('hide');
			}
		}
  		$scope.previuos = function() {
			var curr = $scope.divs[i];
			i = i - 1;
			var prev = $scope.divs[i];
			if(i == 0)	{
				$('#'+curr).addClass('hide');	
				$('#'+prev).removeClass('hide');
				$('#previuos').addClass('hide');			 
			 }
			 else if(i ==9){
				$('#'+curr).addClass('hide');	
				$('#'+prev).removeClass('hide');
				$('#next').removeClass('hide');
			 }
			 else{
				$('#'+curr).addClass('hide');	
				$('#'+prev).removeClass('hide');
			 }
		}
		
		$scope.submit = function(){
			alert(JSON.stringify($scope.loanReport));
		}
}]);





