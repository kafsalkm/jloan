
app.controller('loanCtrl',['$scope','$window','$state','loanService',
function($scope, $window, $state, loanService) {

   $scope.loans =  loanService.getmyLoans();

  $scope.viewLoan = function(loan){
	  var loanstr = JSON.stringify(loan);
	  $window.localStorage['loan'] = loanstr;
	  $state.go('app.single');
  }
}]);
